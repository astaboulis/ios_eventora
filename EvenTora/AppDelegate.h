//
//  AppDelegate.h
//  EventTora
//
//  Created by Angelos Staboulis on 19/12/18.
//  Copyright © 2018 Angelos Staboulis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

