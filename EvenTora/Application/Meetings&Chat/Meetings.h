//
//  Meetings.h
//  EventTora
//
//  Created by Angelos Staboulis on 24/12/18.
//  Copyright © 2018 Angelos Staboulis. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Meetings : UITableViewController
@property (strong,nonatomic) NSArray     *content;

@end

NS_ASSUME_NONNULL_END
