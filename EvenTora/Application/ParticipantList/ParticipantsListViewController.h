//
//  ParticipantsListViewController.h
//  EvenTora
//
//  Created by Angelos Staboulis on 30/12/18.
//  Copyright © 2018 Angelos Staboulis. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ParticipantsListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
- (IBAction)btnClose:(id)sender;
- (IBAction)btnAllContacts:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnAllContacts;
@property (weak, nonatomic) IBOutlet UITextView *txtSearch;
@property (weak, nonatomic) IBOutlet UIButton *btnSearch;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@property (weak, nonatomic) IBOutlet UIButton *btnMultipleContacts;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)btnSearch:(id)sender;

- (IBAction)btnMultipleContacts:(id)sender;

@end

NS_ASSUME_NONNULL_END
