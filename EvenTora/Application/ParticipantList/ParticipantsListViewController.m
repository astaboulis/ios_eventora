//
//  ParticipantsListViewController.m
//  EvenTora
//
//  Created by Angelos Staboulis on 30/12/18.
//  Copyright © 2018 Angelos Staboulis. All rights reserved.
//
#import "MessageViewController.h"

#import "ParticipantsListViewController.h"
#import "ParticipantListCell.h"
@interface ParticipantsListViewController ()
   
@end

@implementation ParticipantsListViewController
NSArray *names;
NSArray *ceo;
- (void)viewDidLoad {
    [super viewDidLoad];
    [_txtSearch.layer setBorderWidth:1.0];
    [_btnAllContacts.layer setBorderWidth:1.0];
    [_btnMultipleContacts.layer setBorderWidth:1.0];
    UINib *cellNib = [UINib nibWithNibName:@"ParticipantListCell" bundle:nil];
    
    [self.tableView registerNib:cellNib forCellReuseIdentifier:@"ParticipantListCell"];
    
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    
    names = [NSArray arrayWithObjects:@"Angelos Staboulis", @"Bill Gates", @"Nick Tsiamis", @"Bill Triantafyllis", nil];
    ceo = [NSArray arrayWithObjects:@"CEO", @"CTO", @"CEO", @"CTO", @"CTO", nil];
    
    // Do any additional setup after loading the view.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [names count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 300.0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"cell";
    
    ParticipantListCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[ParticipantListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    cell.fullname.text=[names objectAtIndex:indexPath.row];
    cell.ceo.text=[ceo objectAtIndex:indexPath.row];
    cell.img.image=[UIImage imageNamed:@"bill"];
    cell.conversationButtonTapHandler = ^{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        MessageViewController *object = [storyboard instantiateViewControllerWithIdentifier:@"MessageViewController"];
        [self presentViewController:object animated:YES completion:nil];
    };
    return cell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnClose:(id)sender {
    [_txtSearch setText:@""];
}

- (IBAction)btnAllContacts:(id)sender {
    [_btnMultipleContacts setBackgroundColor:UIColor.whiteColor];
    [_btnAllContacts setBackgroundColor:UIColor.blueColor];
}


- (IBAction)btnSearch:(id)sender {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",_txtSearch.text];
    names = [names filteredArrayUsingPredicate:predicate];
    [self.tableView reloadData];
}

- (IBAction)btnMultipleContacts:(id)sender {
    [_btnAllContacts setBackgroundColor:UIColor.whiteColor];
    [_btnMultipleContacts setBackgroundColor:UIColor.blueColor];
}
@end
