//
//  QuestionsCategoriesController.h
//  EvenTora
//
//  Created by Angelos Staboulis on 3/1/19.
//  Copyright © 2019 Angelos Staboulis. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QuestionsCategoriesController : UITableViewController
@property (strong, nonatomic) NSArray *categories;
@property (strong, nonatomic) NSArray *categoriesList;
@end

NS_ASSUME_NONNULL_END
