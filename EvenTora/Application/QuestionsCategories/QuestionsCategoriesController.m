//
//  QuestionsCategoriesController.m
//  EvenTora
//
//  Created by Angelos Staboulis on 3/1/19.
//  Copyright © 2019 Angelos Staboulis. All rights reserved.
//

#import "QuestionsCategoriesController.h"
#import "Questions.h"
#import <AFNetworking/AFNetworking.h>
@interface QuestionsCategoriesController ()

@end

@implementation QuestionsCategoriesController

NSMutableArray *categories;

NSMutableArray *categoriesList;

-(void) getQuestionCategories:(NSString*)string withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{
    
    NSString *url = [NSString stringWithFormat:@"http://stage.eventora.com/el/RESTApi/GetQuestionsCategories"];
    NSDictionary *parameters = @{@"eventSlug":@"testiosapp"};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"cDYmgec3PIh3ErLeVLsyMaBxZMOCZXvw" forHTTPHeaderField:@"apiKey"];
    [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionDataTask *task, id responseObject){
        self.categories=[responseObject objectForKey:@"questionCategories"];
        NSString *str =[NSString stringWithFormat:@"%@",responseObject];
        NSLog(@"%@",str);
        if (completion){
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(YES,nil,str); // here that call when method complete
            });
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"Error--: %@", error);
    }];
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    categoriesList = [NSMutableArray arrayWithObjects:@"", nil];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
- (void)viewDidAppear:(BOOL)animated{
    [self getQuestionCategories:@"Questions" withCompletion:^(BOOL success, NSError *error, id responce) {
        NSArray *listCategories = [self.categories valueForKey:@"categoryName"];
        NSEnumerator *e = [listCategories objectEnumerator];
        id object;
        while (object = [e nextObject]) {
            [categoriesList addObject:object];
        }
        
        [self.tableView reloadData];
   }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return categoriesList.count;

}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    int i=0;
    while(i<categoriesList.count){
        cell.textLabel.text=[[categoriesList objectAtIndex:indexPath.row] capitalizedString];
        i=i+1;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    Questions *questions = (Questions *)[storyboard instantiateViewControllerWithIdentifier:@"Questions"];
    [self presentViewController:questions animated:YES completion:nil];

}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
