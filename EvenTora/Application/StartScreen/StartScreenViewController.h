//
//  StartScreenViewController.h
//  EventTora
//
//  Created by Angelos Staboulis on 20/12/18.
//  Copyright © 2018 Angelos Staboulis. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface StartScreenViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *startImage;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet FBSDKLoginButton *btnFacebookLogin;

@property (weak, nonatomic) IBOutlet UITextField *txtUserName;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;

@end

NS_ASSUME_NONNULL_END
