//
//  StartScreenViewController.m
//  EventTora
//
//  Created by Angelos Staboulis on 20/12/18.
//  Copyright © 2018 Angelos Staboulis. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "StartScreenViewController.h"
#import "MainTabBar.h"
@interface StartScreenViewController ()

@end

@implementation StartScreenViewController
BOOL moved;
-(void) createRoundedButton{
  
    _btnLogin.layer.cornerRadius = 10;
    _btnLogin.layer.borderWidth = 5;
    _btnLogin.layer.borderColor = [UIColor whiteColor].CGColor;
    _btnFacebookLogin.layer.cornerRadius = 10;
    _btnFacebookLogin.layer.borderWidth = 5;
    _btnFacebookLogin.layer.borderColor = [UIColor clearColor].CGColor;

}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_txtUserName resignFirstResponder];
    [_txtPassword resignFirstResponder];
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField==_txtUserName)
    {
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-150, self.view.frame.size.width, self.view.frame.size.height)];
    }
    if(textField==_txtPassword)
    {
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-150, self.view.frame.size.width, self.view.frame.size.height)];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self.view setFrame:CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height)];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.startImage setFrame:CGRectMake(0, 0, UIScreen.mainScreen.bounds.size.width, UIScreen.mainScreen.bounds.size.height)];
    [self createRoundedButton];
    [_txtUserName setDelegate:self];
    [_txtPassword setDelegate:self];
    [_txtUserName setKeyboardType:UIKeyboardTypeAlphabet];
    [_txtPassword setKeyboardType:UIKeyboardTypeAlphabet];
    [_txtPassword setSecureTextEntry:TRUE];
    // Optional: Place the button in the center of your view.
   
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
