//
//  ParticipantListCell.h
//  EventTora
//
//  Created by Angelos Staboulis on 28/12/18.
//  Copyright © 2018 Angelos Staboulis. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ParticipantListCell : UITableViewCell
- (IBAction)btnConversation:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *fullname;
@property (weak, nonatomic) IBOutlet UILabel *ceo;
@property (nonatomic, copy) void(^conversationButtonTapHandler)(void);
@end

NS_ASSUME_NONNULL_END
