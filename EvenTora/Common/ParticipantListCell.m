//
//  ParticipantListCell.m
//  EventTora
//
//  Created by Angelos Staboulis on 28/12/18.
//  Copyright © 2018 Angelos Staboulis. All rights reserved.
//

#import "ParticipantListCell.h"

@implementation ParticipantListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)btnConversation:(id)sender {
    self.conversationButtonTapHandler();
}
@end
