//
//  Questions.h
//  EventTora
//
//  Created by Angelos Staboulis on 20/12/18.
//  Copyright © 2018 Angelos Staboulis. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Questions : UITableViewController<UITableViewDelegate, UITableViewDataSource,UIGestureRecognizerDelegate>
- (IBAction)btnSend:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;

@property (strong, atomic) NSMutableDictionary *dict;
@property (weak, nonatomic) IBOutlet UITextView *question;
@property (strong, nonatomic) NSArray *questions;
@property (weak, nonatomic) IBOutlet UIView *headerView;


@end

NS_ASSUME_NONNULL_END
