//
//  Questions.m
//  EventTora
//
//  Created by Angelos Staboulis on 20/12/18.
//  Copyright © 2018 Angelos Staboulis. All rights reserved.
//

#import "Questions.h"
#import "QuestionsTableViewCell.h"
#import <AFNetworking/AFNetworking.h>
#import "MBProgressHUD.h"
@interface Questions ()

@end

@implementation Questions
NSMutableArray *fullnameList;
NSMutableArray *likesList;

NSMutableArray *questionList;
NSMutableArray *countLikes;
typedef void(^SuccessBlock) (NSArray *responseArray);
    int _clickCounter;
    int tap=0;
    NSInteger previouslySelected;
-(void) getQuestions:(NSString*)string withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{
     __block id json;

    NSString *url = [NSString stringWithFormat:@"http://stage.eventora.com/el/RESTApi/GetQuestions"];
    NSDictionary *parameters = @{@"eventSlug":@"testiosapp",@"categoryId": @(512)};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"cDYmgec3PIh3ErLeVLsyMaBxZMOCZXvw" forHTTPHeaderField:@"apiKey"];
    [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionDataTask *task, id responseObject){
        self.questions=[responseObject objectForKey:@"questionCategory"];
        NSString *str =[NSString stringWithFormat:@"%@",responseObject];
          NSLog(@"ResponseObject:%@",str);
        if (completion){
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(YES,nil,str); // here that call when method complete
            });
        }

    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"Error--: %@", error);
    }];
   
}

-(void) addQuestions:(NSString *) question{
   
    NSString *url = [NSString stringWithFormat:@"http://stage.eventora.com/el/RESTApi/AddQuestion"];
    NSDictionary *parameters = @{@"eventSlug":@"testiosapp",@"categoryId": @(512),@"question":question,@"uid":@"123456"};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
     manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"cDYmgec3PIh3ErLeVLsyMaBxZMOCZXvw" forHTTPHeaderField:@"apiKey"];
    [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionDataTask *task, id responseObject){
      
        [self.question setText:@""];
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.headerView animated:YES];
        
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.label.text = NSLocalizedString(@"Η αποθήκευση της εγγραφής έγινε....", @"Ενημερωτικό Μήνυμα");
        // Move to bottm center.
        hud.offset = CGPointMake(0.f, MBProgressMaxOffset);
        
        [hud hideAnimated:YES afterDelay:3.f];

    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"Error--: %@", error);
    }];
    NSLog(@"tap=%d",tap);
    if(tap>0){
        _clickCounter=0;
    }
    tap=tap+1;
   


   
}
- (void) showalert{
    UIAlertController* alert = [UIAlertController
                                alertControllerWithTitle:@"Ενημερωτικό Μήνυμα"
                                message:@"H εγγραφή καταχωρήθηκε!!"
                                preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    [self presentViewController:alert animated:YES completion:nil];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)),     dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:nil];
    });
}
- (void)viewDidLoad {
    [super viewDidLoad];
    tap=0;
    self.dict = [[NSMutableDictionary alloc] init];
    questionList = [NSMutableArray arrayWithObjects:@"", nil];
    likesList = [NSMutableArray arrayWithObjects:@"", nil];
  
    [self.tableView registerNib:[UINib nibWithNibName:@"QuestionsTableViewCell" bundle:nil]
         forCellReuseIdentifier:@"cell"];
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
    [self.question.layer setBorderWidth:1.0f];
    [self.question.layer setBorderColor:[[UIColor grayColor] CGColor]];
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(questionPressed:)];
    gestureRecognizer.delegate = self;
    [_question addGestureRecognizer:gestureRecognizer];

}
- (IBAction)questionPressed:(id)sender {
    _question.text=@"";
}
- (void)viewDidAppear:(BOOL)animated {
        [self getQuestions:@"Questions" withCompletion:^(BOOL success, NSError *error, id responce) {
            NSArray *questionArray = [self.questions valueForKey:@"questions"];
            NSArray *question1Array = [questionArray valueForKey:@"question"];
            NSArray *likesArray = [questionArray valueForKey:@"likes"];
            NSEnumerator *e = [question1Array objectEnumerator];
            id object;
            while (object = [e nextObject]) {
                [questionList addObject:object];
            }
            NSEnumerator *e1 = [likesArray objectEnumerator];
            id object1;
            while (object1 = [e1 nextObject]) {
                [likesList addObject:object1];
            }
            [self.tableView reloadData];
        }];
}

    //[self getQuestionCategories];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

     return questionList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    QuestionsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.lblFullName.text = [[fullnameList objectAtIndex:0] capitalizedString];
    [cell.btnLike setEnabled:FALSE];
    int i=0;
    while(i<questionList.count){
        cell.lblQuestion.text=[[questionList objectAtIndex:indexPath.row] capitalizedString];
        cell.lblQuestion.numberOfLines=0;
        cell.lblQuestion.lineBreakMode=NSLineBreakByWordWrapping;
        i=i+1;
    }
    if(indexPath.row<likesList.count){
        NSString *clickCounterString=[NSString stringWithFormat:@"%@",[likesList objectAtIndex:indexPath.row]];
        cell.lblCountLikes.text=clickCounterString;

    }
   
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm"];
    cell.lblDate.text=[dateFormatter stringFromDate:[NSDate date]];
    cell.lblDate.textColor=UIColor.lightGrayColor;
    [cell.lblDate setFont:[UIFont fontWithName:@"Helvetica" size:14]];

    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 150;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    QuestionsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.lblFullName.text = [[fullnameList objectAtIndex:0] capitalizedString];
    int i=0;
    while(i<questionList.count){
        cell.lblQuestion.text=[[questionList objectAtIndex:indexPath.row] capitalizedString];
        cell.lblQuestion.numberOfLines=0;
        cell.lblQuestion.lineBreakMode=NSLineBreakByWordWrapping;
        i=i+1;
    }
    
    cell.likeButtonTapHandler = ^{
              int like=[[likesList objectAtIndex:indexPath.row] intValue];
             _clickCounter=like+(_clickCounter);
             NSString *clickCounterString = [NSString stringWithFormat:@"%d", _clickCounter];
             cell.lblCountLikes.text=clickCounterString;
             
     
        
    };
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm"];

    cell.lblDate.text=[dateFormatter stringFromDate:[NSDate date]];
    
    [cell.btnLike setEnabled:TRUE];

    previouslySelected=indexPath.row;
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here, for example:
    // Create the next view controller.
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnSend:(id)sender {
   
    [questionList addObject:_question.text];
    [self addQuestions:_question.text];
    [self.tableView reloadData];
    tap=1;
  
}



@end
