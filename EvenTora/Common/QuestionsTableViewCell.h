//
//  QuestionsTableViewCell.h
//  EventTora
//
//  Created by Angelos Staboulis on 20/12/18.
//  Copyright © 2018 Angelos Staboulis. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QuestionsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblFullName;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblQuestion;
@property (weak, nonatomic) IBOutlet UIButton *btnLike;
@property (weak, nonatomic) IBOutlet UILabel *lblCountLikes;
@property (nonatomic, copy) void(^likeButtonTapHandler)(void);
- (IBAction)btnLikes:(id)sender;

@end

NS_ASSUME_NONNULL_END
