//
//  QuestionsTableViewCell.m
//  EventTora
//
//  Created by Angelos Staboulis on 20/12/18.
//  Copyright © 2018 Angelos Staboulis. All rights reserved.
//

#import "QuestionsTableViewCell.h"

@implementation QuestionsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)btnLikes:(id)sender {
    self.likeButtonTapHandler();

}
@end
